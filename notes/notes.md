
# Getting started

    apt-get install nodejs  # for rails

    rails new blog -m http://emberjs.com/edge_template.rb
    cd blog
    bundle install

    rails g scaffold post title:string text:text author_avatar:string
    rails g scaffold comment text:text author:string
    rake db:migrate

    rails generate ember:bootstrap

Brauch ich das?
check app/models
post
  has_many :comments
comment
  belongs_to :post


    vim config/routes.rb
    # => assets#index

    vim app/views/assets/index.html.erb app/assets/javascripts/application.js

    # application.js : ändern
    // require_tree .
    //= require handlebars
    //= require ember
    //= require ember-data
    //= require_self

# Was will ich heute zeigen:

0. Allgemeines
1. Templates
2. Router
3. Controller
4. Models
5. [NIY]Components

# 0. Allgemeines

## Vorbereitung: Ember Inspector
* Chrome/Chromium Extension

## Api Doku
Resource:
http://emberjs.com/api/classes/Ember.Application.html

# 1. Templates
## Html zu einer Ember Application machen
    vim app/assets/javascripts/application.js

    var Blog = Ember.Application.create({
        LOG_TRANSITIONS: true
    });

    vim app/views/assets/index.html.erb

    <script type="text/x-handlebars">
       <h1>Hi</h1>
    </script>
## Auto-Inception

    <script type="text/x-handlebars">
       <h1>Hi</h1>
       {{outlet}}
    </script>
    <script type="text/x-handlebars" data-template-name='index'>
       <h2>index</h2>
    </script>
## Inception??
    <script type="text/x-handlebars">
       <h1>Hi</h1>
       {{outlet}}
    </script>
    <script type="text/x-handlebars" data-template-name='index'>
       <h2>index</h2>
    </script>
    <script type="text/x-handlebars" data-template-name='weiter'>
       <h2>noch was</h2>
    </script>

# 2. Router

## Unsere erste Route

    vim app/assets/javascripts/application.js

    Blog.Router.map(function(){
      this.route('weiter');
    });

    vim app/views/assets/index.html.erb

    {{#link-to 'weiter'}} weiter {{/link-to}} <br/ >
    {{#link-to 'index'}} start {{/link-to}} <br />

# 3. Controller

## Der erste Controller

    vim app/assets/javascripts/application.js

    Blog.Router.map(function(){
      this.route('weiter');
      this.resource('post');
    });

Warum jetzt 'resource'?

Ideologischer Unterschied:

* resource -> Nomen
* route    -> was man mit dem Nomen macht

Wird interessant bei verschachtelten Routen, z.B.
* /post/create
* /post/delete

Kurzer Vorgriff:

    App.Router.map(function(){
      this.resource("posts", { path: "/"}, function(){
        this.route("new", { path: "/new"});
        });
    });

Aber jetzt erstmal:

    Blog.PostController = Ember.Controller.extend({
      title: "Mein erster Titel",
      text: "Mein erster Post",
      author_avatar: "images/avatar.gif"
    });

    vim app/views/assets/index.html.erb
    
    <script type="text/x-handlebars" data-template-name="post">
      <h2>{{title}}</h2>
      <p>{{text}}</p>
      <img {{bind-attr src=author_avatar}} />
    </script>

## Mit einer berechneten Eigenschaft

    vim app/assets/javascripts/application.js

    Blog.PostController = Ember.Controller.extend({
      title: "Mein erster Titel",
      text: "Mein erster Post",
      author_avatar: "images/avatar.gif",
      displayedAt: function(){
        return (new Date()).toDateString();
      }.property()
    });

    vim app/views/assets/index.html.erb
    
    <script type="text/x-handlebars" data-template-name="post">
      <h2>{{title}}</h2>
      <p>{{text}}</p>
      <img {{bind-attr src=author_avatar}} />
      <p>{{displayedAt}}</p>
    </script>
# 4) Models

    vim app/assets/javascripts/application.js

Die Verbindung zum BackEnd wird über den Store verwaltet:

    Blog.Store = DS.Store.extend({
      //adapter: 'Blog.Spezialadapter'
    });

===>
    Blog.Store.find("post", 123)
    ->
    /post/123

Es gibt verschiedene Adapert für das BackEnd.

Standard:
http://emberjs.com/guides/models/the-rest-adapter/

Für automatische snake_case - Übersetzung:
http://http://emberjs.com/api/data/classes/DS.ActiveModelAdapter.html

snakeCase -> snake_case

    App.ApplicationAdapter = DS.ActiveModelAdapter.extend({
      //namespace: '/api'
    });

http://emberjs.com/guides/models/defining-models/
--> Standarddatentypen:

* string
* number
* boolean
* date

    Blog.Posts = DS.Model.extend({
      title: DS.attr('string'),
      text: DS.attr(),
      authorAvatar: DS.attr('string')
    });

// mit "s"

Um das Model zu nutzen, müssen wir die Routen anpassen

    Blog.Router.map(function(){
      //..
      this.resource('posts', function(){
        this.resource('post', {path: '/:post_id'})
        });
    });

und das Model hinterlegen

    Blog.PostsRoute = Ember.Route.extend({
      model: function(params){
        return Blog.Store.find('posts', params.post_id);
      }
    });

... Keine Zeit mehr, muss los *:-/ 
