# for more details see: http://emberjs.com/guides/models/defining-models/

Blog.Comment = DS.Model.extend
  text: DS.attr 'string'
  author: DS.attr 'string'
