#= require jquery
#= require handlebars
#= require ember
#= require ember-data
#= require_self
# require blog

# for more details see: http://emberjs.com/guides/application/
Window.Blog = Ember.Application.create({
  LOG_TRANSITIONS: true
});

