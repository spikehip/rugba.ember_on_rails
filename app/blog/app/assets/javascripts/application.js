// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
// require_tree .
//= require handlebars
//= require ember
//= require ember-data
//= require_self

var Blog = Ember.Application.create({
  LOG_TRANSITIONS: true
});

Blog.Router.map(function(){
  this.route('second');
  this.resource('posts', {path: '/posts/:post_id'});
});

Blog.store = DS.Store.extend({

});

Blog.PostsRoute = Ember.Route.extend({
  model: function(param) {
    return this.store.find('post', param.post_id)
  }
});

/*
Blog.PostController = Ember.Controller.extend({
  title: "First Title",
  text: "First Post",
  author_avatar: "http://placekitten.com/150/150",
  displayedAt: function(){
    return (new Date()).toDateString();
  }.property()
});
*/

Blog.ApplicationAdapter = DS.ActiveModelAdapter.extend({
  //namespace: 'api/'
});
Blog.Post = DS.Model.extend({
  title: DS.attr(),
  text: DS.attr('string'),
  authorAvatar: DS.attr('string')
});

//http://localhost:3000/posts/1

/*
var Blog = Ember.Application.create({
  LOG_TRANSITIONS: true
});

Blog.Router.map(function(){
  this.route('weiter');
  this.resource('post');
  this.resource('posts');
}

Blog.PostController = Ember.Controller.extend({
  title: "Mein erster Titel",
  text: "Mein erster Post",
  author_avatar: "images/avatar.gif",
  displayedAt: function(){
    return (new Date()).toDateString();
   }.property()
});

Blog.Store = DS.Store.extend({
  
});

Blog.ApplicationAdapter = DS.ActiveModelAdapter.extend({});

Blog.Posts = DS.Model.extend({
  title: DS.attr('string'),
  text: DS.attr(),
  authorAvatar: DS.attr('string')
});
*/